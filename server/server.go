package server

import (
	"context"
	"crypto/tls"
	"fmt"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"github.com/labstack/gommon/log"
	"os"
	"os/signal"
	"time"
)

type (
	RouteMap []*Route

	RouteMapJSON struct {
		Routes []*Route `json:"routes"`
	}

	BusinessService interface {
		LoadRoutes() RouteMap
	}

	Route struct {
		Method      string `json:"method"`
		Path        string `json:"path"`
		HandlerFunc echo.HandlerFunc `json:"-"`
	}

	Server struct {
		ListenAddress  string
		Echo           *echo.Echo
		TLSCertificate tls.Certificate
		Service		   BusinessService
	}

	AddRouteError struct {
		Route *Route
	}
)

func (ae AddRouteError) Error() string {
	return fmt.Sprintf("Unable to add route: %v %v", ae.Route.Method, ae.Route.Path)
}

func (rm *RouteMap) AddRoute(newRoute Route) error {
	for _, route := range *rm {
		if route.Path == newRoute.Path && route.Method == newRoute.Method {
			return AddRouteError{
				Route: &newRoute,
			}
		}
	}

	*rm = append(*rm, &newRoute)

	return nil
}

// JSON returns a RouteMapJSON struct which wraps the receiving RouteMap type (for encoding/decoding JSON)
func (rm RouteMap) JSON() RouteMapJSON {
	return RouteMapJSON{
		Routes: rm,
	}
}

// NewServer creates a new transport server instance
func NewServer(listenAddress string, service BusinessService) *Server {
	echoServer := echo.New()
	echoServer.Use(middleware.Logger())
	transportServer := Server{
		ListenAddress: listenAddress,
		Echo:          echoServer,
		Service:	   service,
	}

	// Load all the routes
	for _, route := range service.LoadRoutes() {
		transportServer.Echo.Add(route.Method, route.Path, route.HandlerFunc)
	}

	return &transportServer
}

// Start wraps the Echo TLSServer.ListenAndServeTLS function with graceful shutdown of the HTTP server.
func (srv *Server) Start(certFile, keyFile string, timeout time.Duration) {
	fmt.Printf("[%v] Starting server...\n", time.Now().Local().String())
	if timeout < time.Duration(time.Second) {
		fmt.Printf("[%v] Timeout value less than 1 second, setting timeout to 10 seconds.", time.Now().Local().String())
	}

	srv.Echo.Logger.SetLevel(log.DEBUG)
	srv.Echo.TLSServer.Addr = srv.ListenAddress
	go func() {
		if err := srv.Echo.TLSServer.ListenAndServeTLS(certFile, keyFile); err != nil {
			srv.Echo.Logger.Infof("shutting down the server, received %v", err.Error())
		}
	}()

	// Wait for interrupt signal to gracefully shutdown
	quit := make(chan os.Signal)
	signal.Notify(quit, os.Interrupt)
	<-quit
	ctx, cancel := context.WithTimeout(context.Background(), timeout)
	defer cancel()
	if err := srv.Echo.Shutdown(ctx); err != nil {
		srv.Echo.Logger.Fatal(err)
	}
}
