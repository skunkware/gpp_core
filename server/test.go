package server

import (
	"crypto/ecdsa"
	"crypto/elliptic"
	"crypto/rand"
	"crypto/x509"
	"crypto/x509/pkix"
	"encoding/pem"
	"github.com/labstack/echo/v4"
	"github.com/labstack/gommon/log"
	"math/big"
	"net"
	"net/http"
	"os"
	"time"
)

type (
	TestService struct {

	}
)

func NewTestService() BusinessService {
	return &TestService{}
}

func (s *TestService) LoadRoutes() (rm RouteMap) {
	rootContext := "/api/v1"
	rm = make(RouteMap, 0)

	_ = rm.AddRoute(Route{
		Method: "GET",
		Path: "/status",
		HandlerFunc: s.statusHandler,
	})

	_ = rm.AddRoute(Route{
		Method: "GET",
		Path: rootContext + "/test",
		HandlerFunc: s.testHandler,
	})

	return
}

func (s *TestService) statusHandler(ctx echo.Context) error {
	return ctx.JSON(http.StatusOK, "OK")
}

func (s *TestService) testHandler(ctx echo.Context) error {
	return ctx.JSON(http.StatusOK, "OK")
}

func CleanUpTLSCertificate() {
	files := [2]string{"key.pem", "cert.pem",}
	for _, file := range files {
		if frmErr := os.Remove(file); frmErr != nil {
			// Do something about it
		}
	}
}

func GenerateTLSCertificate() {
	var priv *ecdsa.PrivateKey
	var err error
	priv, err = ecdsa.GenerateKey(elliptic.P256(), rand.Reader)

	if err != nil {
		log.Fatalf("failed to generate private key: %s", err)
	}

	serialNumberLimit := new(big.Int).Lsh(big.NewInt(1), 128)
	serialNumber, err := rand.Int(rand.Reader, serialNumberLimit)
	if err != nil {
		log.Fatalf("failed to generate serial number: %s", err)
	}

	template := x509.Certificate{
		SerialNumber: serialNumber,
		Subject: pkix.Name{
			Organization: []string{"Acme Co"},
		},
		NotBefore: time.Now(),
		NotAfter:  time.Now().AddDate(0, 1, 0),
		KeyUsage:              x509.KeyUsageKeyEncipherment | x509.KeyUsageDigitalSignature,
		ExtKeyUsage:           []x509.ExtKeyUsage{x509.ExtKeyUsageServerAuth},
		BasicConstraintsValid: true,
	}

	template.IPAddresses = append(template.IPAddresses, net.ParseIP("127.0.0.1"))
	template.IsCA = true
	template.KeyUsage |= x509.KeyUsageCertSign

	derBytes, err := x509.CreateCertificate(rand.Reader, &template, &template, priv.Public(), priv)
	if err != nil {
		log.Fatalf("Failed to create certificate: %s", err)
	}

	certOut, err := os.Create("cert.pem")
	if err != nil {
		log.Fatalf("failed to open cert.pem for writing: %s", err)
	}
	if err := pem.Encode(certOut, &pem.Block{Type: "CERTIFICATE", Bytes: derBytes}); err != nil {
		log.Fatalf("failed to write data to cert.pem: %s", err)
	}
	if err := certOut.Close(); err != nil {
		log.Fatalf("error closing cert.pem: %s", err)
	}

	keyOut, err := os.OpenFile("key.pem", os.O_WRONLY|os.O_CREATE|os.O_TRUNC, 0600)
	if err != nil {
		log.Print("failed to open key.pem for writing:", err)
		return
	}

	privB, _ := x509.MarshalECPrivateKey(priv)
	if err := pem.Encode(keyOut, &pem.Block{Type: "EC PRIVATE KEY", Bytes: privB}); err != nil {
		log.Fatalf("failed to write data to key.pem: %s", err)
	}

	if err := keyOut.Close(); err != nil {
		log.Fatalf("error closing key.pem: %s", err)
	}
}