package server

import (
	"context"
	"testing"
	"time"
)

func TestServer(t *testing.T) {
	bs := NewTestService()
	GenerateTLSCertificate()
	t.Run("TestNewServer", testNewServer(bs))
	CleanUpTLSCertificate()
}

func testNewServer(bs BusinessService) func(t *testing.T) {
	return func(t *testing.T) {
		srv := NewServer(":8091", bs)
		go srv.Start("./cert.pem", "./key.pem", time.Second * 10)
		if err := srv.Echo.Shutdown(context.Background()); err != nil {
			t.Logf("Failed to shutdown server: %v", err)
			t.Fail()
		}

	}
}