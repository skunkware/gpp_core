package plant

import (
	"context"
	"gpp/core"
	dbTest "gpp/store/test"
	"testing"
)

var (
	testCtx = context.TODO()
	TestPlants = []*core.Plant{
		{Name: "Mighty Myrcene", Terpenes: core.Terpenes{Myrcene: 0.05}},
		{Name: "Testy Terpster", Terpenes: core.Terpenes{AlphaPinene: 0.015, BetaPinene: 0.02, Myrcene: 0.03}},
	}
)

func TestPlant(t *testing.T) {
	conn, _ := dbTest.Connect()
	store := NewStore(conn).(*dataStore)
	defer dbTest.Disconnect(conn)

	t.Run("Create", testPlantCreate(store))
}

func testPlantCreate(store *dataStore) func(t *testing.T) {
	return func(t *testing.T) {
		for _, tp := range TestPlants {
			err := store.Create(testCtx, tp)
			if err != nil {
				t.Error(err)
			}

			if fErr := store.Find(testCtx, tp); fErr != nil {
				t.Error(fErr)
			}
		}

		if TestPlants[0].ID != 1 {
			t.Errorf("Expected %+v to have ID == 1, got %d", TestPlants[0], TestPlants[0].ID)
		}

		if TestPlants[1].ID != 2 {
			t.Errorf("Expected %+v to have ID == 2, got %d", TestPlants[1], TestPlants[1].ID)
		}
	}
}
