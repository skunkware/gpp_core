package plant

import (
	"context"
	"github.com/jinzhu/gorm"
	"gpp/core"
)

type (
	dataStore struct {
		db *gorm.DB
	}
)

func NewStore(db *gorm.DB) core.PlantStore {
	return &dataStore{
		db: db,
	}
}

func (ds *dataStore) Create(ctx context.Context, plant *core.Plant) error {
	result := ds.db.Create(plant)
	if result.Error != nil {
		return result.Error
	}

	return nil
}

func (ds *dataStore) Find(ctx context.Context, plant *core.Plant) error {
	result := ds.db.Where("name = ?", plant.Name).Find(plant)
	if result.Error != nil {
		return result.Error
	}

	result.Assign()

	return nil
}