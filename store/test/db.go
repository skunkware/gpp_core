package test

import (
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
	"gpp/core"
	"gpp/store/database"
	"os"
	"strings"
)

const (
	driver = "sqlite3"
	datasourceFile = "gorm.db"
)

var datasource string

func Connect() (*gorm.DB, error) {
	if !strings.HasSuffix(os.TempDir(), "/") {
		datasource = os.TempDir() + "/" + datasourceFile
	} else {
		datasource = os.TempDir() + datasourceFile
	}

	_, dsfErr := os.Create(datasource)
	if dsfErr != nil {
		return nil, dsfErr
	}

	db, dbErr := database.Connect(driver, datasource)
	if dbErr != nil {
		return nil, dbErr
	}

	db.AutoMigrate(
		&core.User{},
		&core.Plant{},
		&core.Cultivar{})

	return db, nil
}

func Disconnect(db *gorm.DB) {
	_ = db.Close()
	_ = os.Remove(datasource)
}