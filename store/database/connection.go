package database

import "github.com/jinzhu/gorm"

func Connect(driver, datasource string) (*gorm.DB, error) {
	return gorm.Open(driver, datasource)
}