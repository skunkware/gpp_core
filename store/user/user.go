package user

import (
	"context"
	"github.com/jinzhu/gorm"
	"gpp/core"
)

type (
	dataStore struct {
		db *gorm.DB
	}
)

func NewStore(db *gorm.DB) core.UserStore {
	return &dataStore{
		db: db,
	}
}

func (ds *dataStore) Create(ctx context.Context, login, email string) (*core.User, error) {
	user := core.User{
		Login: login,
		Email: email,
	}

	result := ds.db.Create(&user)
	if result.Error != nil {
		return nil, result.Error
	}

	return &user, nil
}

func (ds *dataStore) Find(ctx context.Context, id uint) (*core.User, error) {
	user := core.User{}
	result := ds.db.First(&user, id)
	if result.Error != nil {
		return nil, result.Error
	}

	return &user, nil
}

func (ds *dataStore) FindByLogin(ctx context.Context, login string) (*core.User, error) {
	user := core.User{}
	result := ds.db.Where("name = ?", login).Find(&user)
	if result.Error != nil {
		return nil, result.Error
	}

	return &user, nil
}