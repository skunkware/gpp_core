package user

import (
	"context"
	dbTest "gpp/store/test"
	"testing"
)

var testCtx = context.TODO()

func TestUser(t *testing.T) {
	conn, _ := dbTest.Connect()
	store := NewStore(conn).(*dataStore)
	defer dbTest.Disconnect(store.db)

	t.Run("Create", testUserCreate(store))
}

func testUserCreate(store *dataStore) func(t *testing.T) {
	return func(t *testing.T) {
		login := "tester"
		email := "tester@test.net"

		user, crErr := store.Create(testCtx, login, email)
		if crErr != nil {
			t.Error(crErr)
		}

		if user.ID == 0 {
			t.Errorf("User ID not assigned, got %d", user.ID)
		}

		t.Run("Find", testUserFind(store, user.ID))
	}
}

func testUserFind(store *dataStore, id uint) func(t *testing.T) {
	return func(t *testing.T) {
		user, err := store.Find(testCtx, id)
		if err != nil {
			t.Error(err)
		} else {
			if user.Email != "tester@test.net" {
				t.Errorf("Expected user with ID == 1 to have Email == tester@test.net, got %s", user.Email)
			}
		}
	}
}