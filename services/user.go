package services

import (
	"context"
	"gpp/core"
)

type (
	userService struct {
		store core.UserStore
	}
)

func NewUserService(store core.UserStore) core.UserService {
	return &userService{
		store: store,
	}
}

func (svc *userService) Create(ctx context.Context, login, email string) (*core.User, error) {
	return &core.User{}, nil
}

func (svc *userService) Find(ctx context.Context, id uint) (*core.User, error) {
	//svc.store.Find()
	return &core.User{}, nil
}

func (svc *userService) FindByLogin(ctx context.Context, login string) (*core.User, error) {
	return &core.User{}, nil
}