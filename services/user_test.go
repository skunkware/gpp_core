package services

import (
	"context"
	"gpp/core"
	dbTest "gpp/store/test"
	"gpp/store/user"
	"testing"
)

var testCtx = context.TODO()

var testUsers = []struct {
	login string
	email string
}{
	{login: "tester1", email: "tester1@testers.net"},
	{login: "tester2", email: "tester2@testers.net"},
}

func TestUserService(t *testing.T) {
	db, _ := dbTest.Connect()
	defer dbTest.Disconnect(db)
	store := user.NewStore(db)
	svc := NewUserService(store)

	t.Run("Create", testUserCreate(svc))
}

func testUserCreate(svc core.UserService) func(t *testing.T) {
	return func(t *testing.T) {
		for _, testUser := range testUsers {
			tu, err := svc.Create(testCtx, testUser.login, testUser.email)
			if err != nil {
				t.Error(err)
			}

			if tu.ID != 0 {
				t.Errorf("TestUser ID is %d, expected %d", tu.ID, 0)
			}
		}
	}
}

func testUserFind(svc core.UserService) func(t *testing.T) {
	return func(t *testing.T) {

	}
}