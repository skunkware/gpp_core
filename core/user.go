package core

import (
	"context"
	"github.com/jinzhu/gorm"
)

type (
	User struct {
		gorm.Model
		Login 	string	`json:"login"`
		Token   string  `json:"-"`
		Email	string	`json:"email"`
	}

	UserStore interface {
		Create(context.Context, string, string) (*User, error)
		Find(context.Context, uint) (*User, error)
		FindByLogin(context.Context, string) (*User, error)
	}

	UserService interface {
		Create(ctx context.Context, login, email string) (*User, error)
		Find(ctx context.Context, id uint) (*User, error)
		FindByLogin(ctx context.Context, login string) (*User, error)
	}
)

