package core

import (
	"context"
	"github.com/jinzhu/gorm"
)

type (
	Terpenes struct {
		AlphaPinene float32
		BetaPinene  float32
		Limonene	float32
		Linalool	float32
		Ocimene		float32
		Myrcene		float32
	}

	Plant struct {
		*gorm.Model
		Name 		string
		Terpenes 	Terpenes
	}

	Cultivar struct {
		*gorm.Model
		Name		string		`json:"name"`
		Breeder 	User		`json:"breeder"`
		ParentM		[]*Plant	`json:"parentM"`
		ParentF		[]*Plant	`json:"parentF"`
	}

	PlantStore interface {
		Create(context.Context, *Plant) error
		Find(context.Context, *Plant) error
	}

	CultivarStore interface {
		Create(context.Context, *Cultivar) error
	}
)

